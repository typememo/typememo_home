# --------------------------------------------
# ZSH manager
# --------------------------------------------
autoload -U compinit && compinit
autoload -Uz vcs_info
setopt prompt_subst
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' stagedstr "%F{yellow}!"
zstyle ':vcs_info:git:*' unstagedstr "%F{red}+"
zstyle ':vcs_info:*' formats "%F{green}%c%u[%b]%f"
zstyle ':vcs_info:*' actionformats '[%b|%a]'
precmd () { vcs_info }
# %F{green}%D{%Y.%m.%d} %T%f
PROMPT='
%F{magenta}[%~]%f ${vcs_info_msg_0_}
$ '
. $HOME/.git-prompt.sh
# See https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh


# --------------------------------------------
# alias
# --------------------------------------------
alias ls="ls -G"
alias l="ls -G"
alias la="ls -Ga"


# --------------------------------------------
# Screenshot Settings
# --------------------------------------------
defaults write com.apple.screencapture location $HOME/Downloads/
killall SystemUIServer


# --------------------------------------------
# PATH SETTINGS
# --------------------------------------------
# DEFAULT PATH
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
# CUSTOM PATH
PATH=$PATH:/Library/TeX/texbin
PATH=$PATH:/opt/X11/bin
PATH=$PATH:/Users/takeru/Library/Android/sdk/platform-tools
PATH=$PATH:/Users/takeru/chromium/depot_tools
PATH=$PATH:/Users/takeru/work/nfbe/depot_tools
PATH=$PATH:/Users/takeru/work/nfbe/src/third_party/depot_tools


# --------------------------------------------
# IDL_PATH SETTINGS
# --------------------------------------------
. "/Applications/harris/idl86/bin/idl_setup.bash"
IDL_PATH="<IDL_DEFAULT>":


# --------------------------------------------
# EXPORT
# --------------------------------------------
export PATH=${PATH}
export IDL_PATH=${IDL_PATH}


# --------------------------------------------
# asdf manager
# --------------------------------------------
. $HOME/.asdf/asdf.sh


# --------------------------------------------
# Direnv
# --------------------------------------------
eval "$(direnv hook zsh)"

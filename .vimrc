"--------------------------------------------------
"Appearance
"--------------------------------------------------
syntax on
set number
set title
set showmatch
set textwidth=80
set ruler

"--------------------------------------------------
"Search
"--------------------------------------------------
set ignorecase
set wrapscan

